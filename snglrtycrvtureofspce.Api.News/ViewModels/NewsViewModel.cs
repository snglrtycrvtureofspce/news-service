﻿using System;
using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Api.News.ViewModels;

public class NewsViewModel : IEntity
{
    #region IEntity
    
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    
    #endregion
    
    public string Title { get; set; }
    
    public string Content { get; set; }
    
    public string PhotoUrl { get; set; }
    
    public Guid UserId { get; set; }
}