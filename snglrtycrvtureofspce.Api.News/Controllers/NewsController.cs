﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Api.News.Handlers.NewsController.CreateNews;
using snglrtycrvtureofspce.Api.News.Handlers.NewsController.DeleteNews;
using snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNews;
using snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNewsList;
using snglrtycrvtureofspce.Api.News.Handlers.NewsController.UpdateNews;
using snglrtycrvtureofspce.Core.Filters;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.Api.News.Controllers;

[ApiController]
[Route("[controller]")]
[Microsoft.AspNetCore.Authorization.Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces("application/json")]
public class NewsController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to create a news item.
    /// </summary>
    /// <param name="request">The request object containing the details of the news to be created.</param>
    /// <returns>A newly created news item.</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /CreateNews
    ///     {
    ///        "title": "Title",
    ///        "content": "Content"
    ///     }
    /// 
    /// </remarks>
    /// <response code="201">Returns the newly created news item.</response>
    [HttpPost(Name = "CreateNews")]
    [HasAccessAuthorization]
    [SwaggerResponse(statusCode: StatusCodes.Status201Created, type: typeof(CreateNewsResponse))]
    public async Task<IActionResult> CreateNews([FromForm] CreateNewsRequest request)
    { 
        request.UserId = User.Claims.GetUserId();
        return Ok(await sender.Send(request));
    }
    
    /// <summary>
    /// The method provider possibility to get news by id.
    /// </summary>
    /// <param name="id">Identifier of the news to be received.</param>
    /// <returns>A news item corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the news item.</response>
    /// <response code="404">If the news is not found.</response>
    [HttpGet("{id:guid}", Name = "GetNews")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetNewsResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetNews(Guid id) => Ok(await sender.Send(new GetNewsRequest { Id = id }));
    
    /// <summary>
    /// The method provider possibility to get a news list.
    /// </summary>
    /// <returns>A list of all available news.</returns>
    /// <response code="200">Returns the list of news.</response>
    [HttpGet(Name = "GetNewsList")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetNewsListResponse))]
    public async Task<IActionResult> GetNewsList() => Ok(await sender.Send(new GetNewsListRequest()));

    /// <summary>
    /// The method provider possibility to update news by id.
    /// </summary>
    /// <param name="id">Identifier of the news to be received.</param>
    /// <param name="request">The request object containing the details of the news to be updated.</param>
    /// <returns>The updated news item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /UpdateNews/{id}
    ///     {
    ///        "title": "Title",
    ///        "content": "Content"
    ///     }
    ///
    /// </remarks>
    /// <response code="200">Returns the updated news item.</response>
    /// <response code="404">If the news is not found.</response>
    [HttpPut("{id:guid}", Name = "UpdateNews")]
    [HasAccessAuthorization]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateNewsResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateNews(Guid id, [FromForm] UpdateNewsRequest request)
    {
        request.Id = id;
        return Ok(await sender.Send(request));
    }

    /// <summary>
    /// The method provider possibility to delete news by id.
    /// </summary>
    /// <param name="id">Identifier of the news to be received.</param>
    /// <returns>A confirmation of the deletion.</returns>
    /// <response code="200">Returns the confirmation of the deleted news.</response>
    /// <response code="404">If the news is not found.</response>
    [HttpDelete("{id:guid}", Name = "DeleteNews")]
    [HasAccessAuthorization]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(DeleteNewsResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteNews(Guid id) => Ok(await sender.Send(new DeleteNewsRequest { Id = id }));
}