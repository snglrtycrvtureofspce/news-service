﻿using snglrtycrvtureofspce.Api.News.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.CreateNews;

public class CreateNewsResponse : ItemResponse<NewsViewModel>;