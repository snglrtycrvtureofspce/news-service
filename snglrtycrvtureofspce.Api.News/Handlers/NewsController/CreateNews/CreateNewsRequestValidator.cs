﻿using FluentValidation;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.CreateNews;

public class CreateNewsRequestValidator : AbstractValidator<CreateNewsRequest>
{
    public CreateNewsRequestValidator()
    {
        RuleFor(command => command.Title)
            .MaximumLength(255).WithMessage("Title has a maximum length of 255.")
            .NotNull().WithMessage("Title cannot be null.")
            .NotEmpty().WithMessage("Title cannot be empty.");
        
        RuleFor(command => command.Content)
            .MaximumLength(5000).WithMessage("Content has a maximum length of 5000.")
            .NotNull().WithMessage("Content cannot be null.")
            .NotEmpty().WithMessage("Content cannot be empty.");
    }
}