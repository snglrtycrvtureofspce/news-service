﻿using System;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.CreateNews;

public class CreateNewsRequest : IRequest<CreateNewsResponse>
{
    public string Title { get; set; }
    
    public string Content { get; set; }
    
    public IFormFile PhotoUrl { get; set; }
    
    public Guid UserId { get; set; }
}