﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Refit;
using snglrtycrvtureofspce.Api.File;
using snglrtycrvtureofspce.Api.News.Data;
using snglrtycrvtureofspce.Api.News.Data.Entities;
using snglrtycrvtureofspce.Api.News.Services.Interfaces;
using snglrtycrvtureofspce.Api.News.ViewModels;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.CreateNews;

public class CreateNewsHandler(INewsRepository repository, IMapperBase mapper, ISystemApi systemApi) 
    : IRequestHandler<CreateNewsRequest, CreateNewsResponse>
{
    public async Task<CreateNewsResponse> Handle(CreateNewsRequest request, CancellationToken cancellationToken)
    {
        string photoUrl = null;
        
        if (request.PhotoUrl != null)
        {
            await using var photoStream = request.PhotoUrl.OpenReadStream();
            var photoStreamPart = new StreamPart(photoStream, request.PhotoUrl.FileName, request.PhotoUrl.ContentType);
            
            var uploadResponse = await systemApi.UploadSystemFilesAsync(photoStreamPart, 
                cancellationToken: cancellationToken);
            photoUrl = uploadResponse.Content.Item.FirstOrDefault()?.Url;
        }
        
        var news = new NewsEntity
        {
            Id = Guid.NewGuid(),
            Title = request.Title,
            Content = request.Content,
            PhotoUrl = photoUrl,
            UserId = request.UserId
        };
        
        await repository.CreateNewsAsync(news, cancellationToken);
        
        var model = mapper.Map<NewsViewModel>(news);
        
        var response = new CreateNewsResponse
        {
            Message = "News have been successfully created.",
            StatusCode = StatusCodes.Status201Created,
            Item = model
        };
        
        return response;
    }
}