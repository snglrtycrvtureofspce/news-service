﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.News.Services.Interfaces;
using snglrtycrvtureofspce.Api.News.ViewModels;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNewsList;

public class GetNewsListHandler(INewsRepository repository, IMapperBase mapper) : IRequestHandler<GetNewsListRequest, 
    GetNewsListResponse>
{
    public async Task<GetNewsListResponse> Handle(GetNewsListRequest listRequest, CancellationToken cancellationToken)
    {
        var news = await repository.GetNewsListAsync();
        
        var models = news.Select(mapper.Map<NewsViewModel>).ToList();
        
        var response = new GetNewsListResponse
        {
            Message = "News list have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Elements = models,
            Total = models.Count
        };
        
        return response;
    }
}