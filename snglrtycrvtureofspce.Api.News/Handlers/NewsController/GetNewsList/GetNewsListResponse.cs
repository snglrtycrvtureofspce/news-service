﻿using snglrtycrvtureofspce.Api.News.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNewsList;

public class GetNewsListResponse : PageViewResponse<NewsViewModel>;