﻿using MediatR;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNewsList;

public class GetNewsListRequest : IRequest<GetNewsListResponse>;