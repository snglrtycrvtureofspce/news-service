﻿using snglrtycrvtureofspce.Api.News.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNews;

public class GetNewsResponse : ItemResponse<NewsViewModel>;