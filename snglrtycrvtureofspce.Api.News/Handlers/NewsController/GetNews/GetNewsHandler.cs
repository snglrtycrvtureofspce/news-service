﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.News.Errors;
using snglrtycrvtureofspce.Api.News.Services.Interfaces;
using snglrtycrvtureofspce.Api.News.ViewModels;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNews;

public class GetNewsHandler(INewsRepository repository, IMapperBase mapper) : IRequestHandler<GetNewsRequest, 
    GetNewsResponse>
{
    public async Task<GetNewsResponse> Handle(GetNewsRequest request, CancellationToken cancellationToken)
    {
        var news = await repository.GetNewsAsync(request.Id) 
                   ?? throw new ValidationException(NewsErrors.NotFound(request.Id));
        
        var model = mapper.Map<NewsViewModel>(news);
        
        var response = new GetNewsResponse
        {
            Message = "News been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };
        
        return response;
    }
}