﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.GetNews;

public class GetNewsRequest : IRequest<GetNewsResponse>
{
    public Guid Id { get; init; }
}