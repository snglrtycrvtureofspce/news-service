﻿using FluentValidation;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.DeleteNews;

public class DeleteNewsRequestValidator : AbstractValidator<DeleteNewsRequest>
{
    public DeleteNewsRequestValidator()
    {
        RuleFor(command => command.Id)
            .NotNull().WithMessage("Id cannot be null.")
            .NotEmpty().WithMessage("Id cannot be empty.");
    }
}