﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.DeleteNews;

public class DeleteNewsResponse : DeleteResponse;