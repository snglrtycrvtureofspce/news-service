﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.News.Errors;
using snglrtycrvtureofspce.Api.News.Services.Interfaces;
using snglrtycrvtureofspce.Core.Middlewares;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.DeleteNews;

public class DeleteNewsHandler(INewsRepository repository) : IRequestHandler<DeleteNewsRequest, DeleteNewsResponse>
{
    public async Task<DeleteNewsResponse> Handle(DeleteNewsRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var news = await repository.GetNewsAsync(request.Id) 
                       ?? throw new ValidationException(NewsErrors.NotFound(request.Id));
            
            await repository.DeleteNewsAsync(news, cancellationToken);
            
            var response = new DeleteNewsResponse
            {
                Message = "News have been successfully deleted.",
                StatusCode = StatusCodes.Status200OK,
                Id = request.Id
            };
            
            return response;
        }
        catch (DbUpdateException ex) 
            when (IsForeignKeyViolationExceptionMiddleware.CheckForeignKeyViolation(ex, out var referencedObject))
        {
            return new DeleteNewsResponse
            {
                Message = $"Unable to delete news. It is referenced by {referencedObject}.",
                StatusCode = StatusCodes.Status400BadRequest,
                Id = request.Id
            };
        }
    }
}