﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.DeleteNews;

public class DeleteNewsRequest : IRequest<DeleteNewsResponse>
{
    public Guid Id { get; init; }
}