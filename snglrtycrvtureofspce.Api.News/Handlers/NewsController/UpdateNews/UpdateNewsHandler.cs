﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Refit;
using snglrtycrvtureofspce.Api.File;
using snglrtycrvtureofspce.Api.News.Errors;
using snglrtycrvtureofspce.Api.News.Services.Interfaces;
using snglrtycrvtureofspce.Api.News.ViewModels;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.UpdateNews;

public class UpdateNewsHandler(INewsRepository repository, IMapperBase mapper, ISystemApi systemApi) 
    : IRequestHandler<UpdateNewsRequest, UpdateNewsResponse>
{
    public async Task<UpdateNewsResponse> Handle(UpdateNewsRequest request, CancellationToken cancellationToken)
    {
        var news = await repository.GetNewsAsync(request.Id) 
                   ?? throw new ValidationException(NewsErrors.NotFound(request.Id));
        
        string photoUrl = null;
        
        if (request.PhotoUrl != null)
        {
            await using var photoStream = request.PhotoUrl.OpenReadStream();
            var photoStreamPart = new StreamPart(photoStream, request.PhotoUrl.FileName, request.PhotoUrl.ContentType);
            
            var uploadResponse = await systemApi.UploadSystemFilesAsync(photoStreamPart, 
                cancellationToken: cancellationToken);
            photoUrl = uploadResponse.Content.Item.FirstOrDefault()?.Url;
        }
        
        news.Title = request.Title;
        news.Content = request.Content;
        news.PhotoUrl = photoUrl;
        
        await repository.UpdateNewsAsync(news, cancellationToken);
        
        var model = mapper.Map<NewsViewModel>(news);
        
        var response = new UpdateNewsResponse
        {
            Message = "News have been successfully updated.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };
        
        return response;
    }
}