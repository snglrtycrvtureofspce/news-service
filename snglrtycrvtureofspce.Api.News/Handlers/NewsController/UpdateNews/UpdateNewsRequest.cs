﻿using System;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.UpdateNews;

public class UpdateNewsRequest : IRequest<UpdateNewsResponse>
{
    public Guid Id { get; set; }
    
    public string Title { get; set; }
    
    public string Content { get; set; }
    
    public IFormFile PhotoUrl { get; set; }
}