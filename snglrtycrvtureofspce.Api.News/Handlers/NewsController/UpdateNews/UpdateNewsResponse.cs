﻿using snglrtycrvtureofspce.Api.News.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.News.Handlers.NewsController.UpdateNews;

public class UpdateNewsResponse : ItemResponse<NewsViewModel>;