﻿using AutoMapper;
using snglrtycrvtureofspce.Api.News.Data.Entities;
using snglrtycrvtureofspce.Api.News.ViewModels;

namespace snglrtycrvtureofspce.Api.News.AutomapperProfiles;

public class NewsProfile : Profile
{
    public NewsProfile()
    {
        CreateMap<NewsEntity, NewsViewModel>();
    }
}