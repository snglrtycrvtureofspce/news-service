﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.News.Data;
using snglrtycrvtureofspce.Api.News.Data.Entities;
using snglrtycrvtureofspce.Api.News.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.News.Services.Implementations;

public class NewsRepository(NewsDbContext context) : INewsRepository
{
    public async Task CreateNewsAsync(NewsEntity news, CancellationToken cancellationToken)
    {
        await context.News.AddAsync(news, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<NewsEntity> GetNewsAsync(Guid id) => await context.News.FindAsync(id);
    
    public async Task<IEnumerable<NewsEntity>> GetNewsListAsync() => await context.News.AsNoTracking().ToListAsync();
    
    public async Task UpdateNewsAsync(NewsEntity news, CancellationToken cancellationToken)
    {
        context.News.Update(news);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteNewsAsync(NewsEntity news, CancellationToken cancellationToken)
    {
        context.News.Remove(news);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}