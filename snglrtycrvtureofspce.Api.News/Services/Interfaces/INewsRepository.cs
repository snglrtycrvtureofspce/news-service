﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using snglrtycrvtureofspce.Api.News.Data.Entities;

namespace snglrtycrvtureofspce.Api.News.Services.Interfaces;

public interface INewsRepository
{
    Task CreateNewsAsync(NewsEntity news, CancellationToken cancellationToken);
    
    Task<NewsEntity> GetNewsAsync(Guid id);
    
    Task<IEnumerable<NewsEntity>> GetNewsListAsync();
    
    Task UpdateNewsAsync(NewsEntity news, CancellationToken cancellationToken);
    
    Task DeleteNewsAsync(NewsEntity news, CancellationToken cancellationToken);
}