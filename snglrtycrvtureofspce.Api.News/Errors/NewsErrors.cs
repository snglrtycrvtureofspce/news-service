﻿using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace snglrtycrvtureofspce.Api.News.Errors;

public static class NewsErrors
{
    public static IEnumerable<ValidationFailure> NotFound(Guid id) => 
        new List<ValidationFailure> { new(nameof(id), $"News not found. Id: {id}") };
}