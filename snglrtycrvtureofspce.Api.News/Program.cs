using System;
using System.Reflection;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using Serilog;
using snglrtycrvtureofspce.Api.File;
using snglrtycrvtureofspce.Api.News.Configurations;
using snglrtycrvtureofspce.Api.News.Data;
using snglrtycrvtureofspce.Api.News.Services.Implementations;
using snglrtycrvtureofspce.Api.News.Services.Interfaces;
using snglrtycrvtureofspce.Core.Filters;
using snglrtycrvtureofspce.Core.Microservices.Core.Configurations;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using snglrtycrvtureofspce.Core.Middlewares;

var builder = WebApplication.CreateBuilder(args);

DotNetEnv.Env.Load();
var connectionString = Environment.GetEnvironmentVariable("DeployConnection");
builder.Services.AddDbContext<NewsDbContext>(options =>
{
    if (connectionString != null) options.UseNpgsql(connectionString);
});

builder.Services.AddServerControllers();
builder.Services.AddSnglrtycrvtureofspceAuthorization();

Log.Logger = new LoggerConfiguration().MinimumLevel.Information().WriteTo.Console().CreateLogger();
builder.Logging.ClearProviders().AddSerilog();

builder.Services.AddMediatR(cfg => { cfg.RegisterServicesFromAssembly(typeof(Program).Assembly); });
builder.Services.AddValidatorsFromAssembly(typeof(Program).Assembly);

var mapperConfig = new MapperConfiguration(p => p.AddMaps(Assembly.GetExecutingAssembly()));
var mapper = mapperConfig.CreateMapper();
builder.Services.AddSingleton(mapper);
builder.Services.AddScoped<IMapperBase>(_ => mapper);

builder.Services.AddTransient(typeof (IPipelineBehavior<,>), typeof (RequestValidationBehavior<,>));

builder.Services.AddTransient(_ => new RefitSettings
{
    ContentSerializer = new NewtonsoftJsonContentSerializer(new JsonSerializerSettings
    {
        ContractResolver = new CamelCasePropertyNamesContractResolver()
    }),
    CollectionFormat = CollectionFormat.Multi
});

builder.Services.AddTransient(sc =>
    RestService.For<ISystemApi>(
        new JwtHttpClient(builder.Configuration.GetMicroserviceHost("FileApi"),
            sc.GetService<IHttpContextAccessor>() 
            ?? throw new InvalidOperationException()), sc.GetService<RefitSettings>()));

builder.Services.AddScoped<INewsRepository, NewsRepository>();

builder.Services.ConfigureApiVersioning();
builder.Services.ConfigureSwagger(builder.Configuration);

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll", corsPolicyBuilder => 
        corsPolicyBuilder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
});

var app = builder.Build();

app.UseRouting();
app.UseCors("AllowAll");

app.UseSwagger();
app.UseSwaggerUI(options =>
{
    foreach (var description in app.DescribeApiVersions())
    {
        options.SwaggerEndpoint($"{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
    }
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.Run();